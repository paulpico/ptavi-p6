#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import os

USAGE = 'python3 server.py IP port audio_file'

try:
    IP_SERVER = sys.argv[1]
    PORT_SERVER = int(sys.argv[2])
    AUDIO_FILE = sys.argv[3]
except IndexError:
    sys.exit('Usage: ' + USAGE)


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        print("La dir IP y puerto del cliente es: " + str(self.client_address))
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()

            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break

            final_line = line.decode('utf-8')
            PETITION = final_line.split()
            print("Recibido: " + final_line)
            if PETITION[0] == 'INVITE':
                self.wfile.write(b"SIP/2.0 100 Trying\r\n\r\n"
                                 + b"SIP/2.0 180 Ringing\r\n\r\n"
                                 + b"SIP/2.0 200 OK\r\n\r\n")
            elif PETITION[0] == 'ACK':
                aEjecutar = 'mp32rtp -i 127.0.0.1 -p 23032 < ' + AUDIO_FILE
                print("Vamos a ejecutar", aEjecutar)
                os.system(aEjecutar)
            elif PETITION[0] == 'BYE':
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            elif PETITION[0] not in ['INVITE', 'ACK', 'BYE']:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
            else:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer((IP_SERVER, PORT_SERVER), EchoHandler)
    print("Listening...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("End Server")
