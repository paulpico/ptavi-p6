#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

USAGE = 'python3 client.py method receiver@IP:SIPort'

try:
    METHOD = sys.argv[1]
    RECEIVER = sys.argv[2].split('@')[0]
    SERVER = sys.argv[2].split('@')[1]
    IP = SERVER.split(':')[0]
    PORT = int(SERVER.split(':')[1])
    LINE = METHOD + ' sip:' + RECEIVER + '@' + IP + ' SIP/2.0\r\n'
except IndexError:
    sys.exit('Usage: ' + USAGE)

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP, PORT))

    print("Enviando: " + LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    try:
        data = my_socket.recv(1024)
        petSIP = data.decode('utf-8').split()
    except ConnectionRefusedError:
        sys.exit("Connection Refused")
    print("Petition Received", data.decode('utf-8'))
    ACK_LINE = 'ACK' + ' sip:' + RECEIVER + '@' + IP + ' SIP/2.0\r\n'
    if METHOD == 'INVITE':
        if petSIP[1] == '100' and petSIP[4] == '180' and petSIP[7] == '200':
            my_socket.send(bytes(ACK_LINE, 'utf-8') + b'\r\n')
    if METHOD == 'BYE':
        if data.decode('utf-8') == "SIP/2.0 200 OK\r\n\r\n":
            print("Terminando socket...")

print("Fin.")
